import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    background-image: linear-gradient(0deg, rgb(245, 255, 189), rgb(229, 184, 255)) !important;
    background-color: #00caff !important;
    overflow: hidden;
    margin: 0;
    padding: 0;
  }
  p{
    margin: 0;
    padding: 0;
  }

  body,ul,ol,li,p,h1,h2,h3,h4,h5,form,fieldset,table,td,img,div,dl,dt,dd,input{margin:0;padding:0;}
  body{font-size:14px;font-family:"微软雅黑";}
  img{border:none;}
  li{list-style:none;}
  input,select,textarea{outline:none;}
  textarea{resize:none;}
  a{text-decoration:none;}

  /*清浮动*/
  .clearfix:after{content:"";display:block;clear:both;}
  .clearfix{zoom:1;}
  #app {
    margin: 10px auto;
    position: relative;
    overflow: hidden;
    border-radius: 20px;
    width: calc(100vw - 20px);
    height: calc(100vh - 20px);
    max-width: 1900px;
    background: linear-gradient(
            to right bottom,
            rgba(255, 255, 255, 0.7),
            rgba(255, 255, 255, 0.3)
    );
    backdrop-filter: blur(2rem);
    box-shadow: rgb(0 0 0 / 15%) 0px 2px 4px -2px;
    transition: all 0.4s ease-in-out 0s;
  }
  
  .c-scrollbar .c-scrollbar_thumb {
    background-color: #faad14 !important;
  }
  
  .notification-class {
    border-radius: 10px !important;
    box-shadow: 0 2px 4px 0 rgb(0 0 0 / 15%) !important;
    background-color: #fcfcfe !important;
  }

  .markdown-body pre,.markdown-body pre>code.hljs{color:#333;background:#f8f8f8}
  .hljs-comment,.hljs-quote{color:#998;font-style:italic}
  .hljs-keyword,.hljs-selector-tag,.hljs-subst{color:#333;font-weight:700}
  .hljs-literal,.hljs-number,.hljs-tag .hljs-attr,.hljs-template-variable,.hljs-variable{color:teal}
  .hljs-doctag,.hljs-string{color:#d14}
  .hljs-section,.hljs-selector-id,.hljs-title{color:#900;font-weight:700}
  .hljs-subst{font-weight:400}
  .hljs-class .hljs-title,.hljs-type{color:#458;font-weight:700}
  .hljs-attribute,.hljs-name,.hljs-tag{color:navy;font-weight:400}
  .hljs-link,.hljs-regexp{color:#009926}.hljs-bullet,.hljs-symbol{color:#990073}
  .hljs-built_in,.hljs-builtin-name{color:#0086b3}
  .hljs-meta{color:#999;font-weight:700}
  .hljs-deletion{background:#fdd}
  .hljs-addition{background:#dfd}
  .hljs-emphasis{font-style:italic}
  .hljs-strong{font-weight:700}
  .markdown-body{word-break:break-word;line-height:1.75;font-weight:400;font-size:15px;overflow-x:hidden;color:#333}
  .markdown-body h1,.markdown-body h2,.markdown-body h3,.markdown-body h4,.markdown-body h5,.markdown-body h6{line-height:1.5;margin-top:35px;margin-bottom:10px;padding-bottom:5px}
  .markdown-body h1{font-size:30px;margin-bottom:5px}
  .markdown-body h2{padding-bottom:12px;font-size:24px;border-bottom:1px solid #ececec}
  .markdown-body h3{font-size:18px;padding-bottom:0}
  .markdown-body h4{font-size:16px}
  .markdown-body h5{font-size:15px}
  .markdown-body h6{margin-top:5px}
  .markdown-body p{line-height:inherit;margin-top:22px;margin-bottom:22px}
  .markdown-body img{max-width:100%}
  .markdown-body hr{border:none;border-top:1px solid #ddd;margin-top:32px;margin-bottom:32px}
  .markdown-body code{word-break:break-word;border-radius:2px;overflow-x:auto;background-color:#fff5f5;color:#ff502c;font-size:.87em;padding:.065em .4em}
  .markdown-body code,.markdown-body pre{font-family:Menlo,Monaco,Consolas,Courier New,monospace}
  .markdown-body pre{overflow:auto;position:relative;line-height:1.75}
  .markdown-body pre>code{font-size:12px;padding:15px 12px;margin:0;word-break:normal;display:block;overflow-x:auto;color:#333;background:#f8f8f8}
  .markdown-body a{text-decoration:none;color:#0269c8;border-bottom:1px solid #d1e9ff}
  .markdown-body a:active,.markdown-body a:hover{color:#275b8c}
  .markdown-body table{display:inline-block!important;font-size:12px;width:auto;max-width:100%;overflow:auto;border:1px solid #f6f6f6}
  .markdown-body thead{background:#f6f6f6;color:#000;text-align:left}
  .markdown-body tr:nth-child(2n){background-color:#fcfcfc}
  .markdown-body td,.markdown-body th{padding:12px 7px;line-height:24px}
  .markdown-body td{min-width:120px}
  .markdown-body blockquote{color:#666;padding:1px 23px;margin:22px 0;border-left:4px solid #cbcbcb;background-color:#f8f8f8}
  .markdown-body blockquote:after{display:block;content:""}
  .markdown-body blockquote>p{margin:10px 0}
  .markdown-body ol,.markdown-body ul{padding-left:28px}
  .markdown-body ol li,.markdown-body ul li{margin-bottom:0;list-style:inherit}
  .markdown-body ol li .task-list-item,.markdown-body ul li .task-list-item{list-style:none}
  .markdown-body ol li .task-list-item ol,.markdown-body ol li .task-list-item ul,.markdown-body ul li .task-list-item ol,.markdown-body ul li .task-list-item ul{margin-top:0}
  .markdown-body ol ol,.markdown-body ol ul,.markdown-body ul ol,.markdown-body ul ul{margin-top:3px}
  .markdown-body ol li{padding-left:6px}
  .markdown-body .contains-task-list{padding-left:0}
  .markdown-body .task-list-item{list-style:none}
  @media (max-width:720px){.markdown-body h1{font-size:24px} .markdown-body h2{font-size:20px}.markdown-body h3{font-size:18px}}
  
  @font-face { 
    font-family: "rainbow"; 
    src: url("http://img.rainbowinpaper.cn/font.svg") format('svg'), 
    url("http://img.rainbowinpaper.cn/font.ttf") format('truetype'), 
    url("http://img.rainbowinpaper.cn/font.woff") format('woff'), 
    url("http://img.rainbowinpaper.cn/font.eot") format('embedded-opentype');
    font-style: normal;
    font-weight: normal;
  }
`;

export default GlobalStyle;
