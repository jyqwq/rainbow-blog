import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowImageCard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowImageCard component!',
  },
});
