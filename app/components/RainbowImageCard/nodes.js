import styled from 'styled-components';

const Container = styled.div``;

const CardImg = styled.div`
  img {
    width: 100%;
  }
`;

const Content = styled.div`
  padding: 0 30px;
  .ant-divider {
    margin: 12px 0;
  }
  .ant-divider-vertical {
    margin: 0 5px;
  }
  span {
    color: #999999;
  }
`;

const SubTitle = styled.div`
  font-size: 1.2rem;
  color: #999999;
  text-align: left;
  line-height: 30px;
`;

const Title = styled.div`
  font-size: 1.5rem;
  color: #3e3e3e;
  text-align: left;
  line-height: 40px;
`;

export default {
  Container,
  CardImg,
  Content,
  SubTitle,
  Title,
};
