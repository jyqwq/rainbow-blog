import React, { memo } from 'react';
// import PropTypes from 'prop-types';

import { FormattedMessage, FormattedRelative } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import { imgBaseUrl } from '../../global-urls';
import { Divider } from 'antd';
import RainbowBaseCard from '../RainbowBaseCard';

function RainbowImageCard() {
  return (
    <RainbowBaseCard
      style={{ margin: '15px' }}
      options={{
        cover: <img alt="home" src={`${imgBaseUrl}home.webp`} />,
      }}
    >
      <Nodes.Container>
        <Nodes.Content>
          <Nodes.SubTitle>Story</Nodes.SubTitle>
          <Nodes.Title>This is a Story Title</Nodes.Title>
          <Divider dashed />
          <FormattedRelative value={Date.now()} />
          <Divider type="vertical" />
          <span>Author</span>
        </Nodes.Content>
      </Nodes.Container>
    </RainbowBaseCard>
  );
}

RainbowImageCard.defaultProps = {};

RainbowImageCard.propTypes = {};

export default memo(RainbowImageCard);
