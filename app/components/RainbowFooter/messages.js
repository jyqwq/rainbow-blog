import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowFooter';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowFooter component!',
  },
});
