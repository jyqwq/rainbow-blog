import React, { memo } from 'react';
// import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import { Row, Col } from 'antd';
import RainbowBaseCard from '../RainbowBaseCard';

function RainbowFooter() {
  return (
    <RainbowBaseCard style={{ margin: '20px 10px' }}>
      <Nodes.Container>
        <Row>
          <Col span={2} />
          <Col span={10}>
            联系我们：
            <a href="https://space.bilibili.com/21648642">纸上的彩虹丶</a>
          </Col>
          <Col span={10}>DESIGN BY: Rainbow In Paper</Col>
          <Col span={2} />
        </Row>
      </Nodes.Container>
    </RainbowBaseCard>
  );
}

RainbowFooter.defaultProps = {};

RainbowFooter.propTypes = {};

export default memo(RainbowFooter);
