import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  height: 40px;
  line-height: 40px;
  color: #999999;
  font-weight: bold;
`;

export default {
  Container,
};
