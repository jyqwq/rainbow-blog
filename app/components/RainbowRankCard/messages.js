import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowRankCard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowRankCard component!',
  },
});
