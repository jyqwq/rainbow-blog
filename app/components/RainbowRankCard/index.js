import React, { memo } from 'react';
// import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import RainbowBaseCard from '../RainbowBaseCard';

function RainbowRankCard() {
  return (
    <RainbowBaseCard style={{ height: 'calc( 100% - 20px )', marginBottom: '30px' }}>
      <Nodes.Container>
        <FormattedMessage {...messages.header} />
      </Nodes.Container>
    </RainbowBaseCard>
  );
}

RainbowRankCard.defaultProps = {};

RainbowRankCard.propTypes = {};

export default memo(RainbowRankCard);
