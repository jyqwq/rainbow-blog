import styled from 'styled-components';

const Shell = styled.div`
  height: 100vh;
  overflow-x: hidden;
  perspective: 3px;
  div {
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 30px;
    letter-spacing: 2px;
  }
`;

const Image = styled.div`
  transform: translateZ(-1px) scale(1.6);
  background-size: cover;
  height: 100vh;
  z-index: -1;
  background-image: url(${props => (props.url ? props.url : '')});
`;

const Text = styled.div`
  height: 50vh;
  background-color: #fff;
  h1 {
    color: #000;
  }
`;

const Heading = styled.div`
  z-index: -1;
  transform: translateY(-30vh) translateZ(1px);
  color: #fff;
  font-size: 30px;
  h1 {
    color: #fff;
  }
`;

export default {
  Shell,
  Image,
  Text,
  Heading,
};
