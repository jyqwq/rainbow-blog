import { defineMessages } from 'react-intl';

export const scope = 'app.components.ParallaxSliding';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ParallaxSliding component!',
  },
});
