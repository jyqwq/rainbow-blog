import React, { memo } from 'react';
// import PropTypes from 'prop-types';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import Nodes from './nodes';
import { imgBaseUrl } from '../../global-urls';

function ParallaxSliding() {
  return (
    <div>
      <Nodes.Shell>
        <Nodes.Image url={`${imgBaseUrl}banner01.jpg`} />
        <Nodes.Heading>
          <h1>When you are confused</h1>
        </Nodes.Heading>
        <Nodes.Text>
          <h1>Set goals in your mind</h1>
        </Nodes.Text>

        <Nodes.Image url={`${imgBaseUrl}banner02.jpg`} />
        <Nodes.Heading>
          <h1>When you're down</h1>
        </Nodes.Heading>
        <Nodes.Text>
          <h1>Try to wake up the beast in your heart</h1>
        </Nodes.Text>

        <Nodes.Image url={`${imgBaseUrl}banner03.jpg`} />
        <Nodes.Heading>
          <h1>When people leave you</h1>
        </Nodes.Heading>
        <Nodes.Text>
          <h1>It's time to start your season</h1>
        </Nodes.Text>
        <Nodes.Image url={`${imgBaseUrl}banner04.jpg`} />
        <Nodes.Heading>
          <h1>Come on,stranger.</h1>
        </Nodes.Heading>
      </Nodes.Shell>
    </div>
  );
}

ParallaxSliding.defaultProps = {};

ParallaxSliding.propTypes = {};

export default memo(ParallaxSliding);
