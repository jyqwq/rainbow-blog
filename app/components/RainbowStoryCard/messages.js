import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowStoryCard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowStoryCard component!',
  },
});
