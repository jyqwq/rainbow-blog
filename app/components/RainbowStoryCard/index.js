import React, { memo } from 'react';
// import PropTypes from 'prop-types';

import { FormattedMessage, FormattedRelative } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import RainbowBaseCard from '../RainbowBaseCard';

function RainbowStoryCard() {
  return (
    <RainbowBaseCard
      style={{
        marginBottom: '30px',
        width: '100%',
        position: 'relative',
        borderRadius: '10px',
        padding: '10px',
      }}
    >
      <Nodes.Container>
        <FormattedMessage {...messages.header} />
        <Nodes.Date>
          <FormattedRelative value={Date.now()} />
        </Nodes.Date>
      </Nodes.Container>
    </RainbowBaseCard>
  );
}

RainbowStoryCard.defaultProps = {};

RainbowStoryCard.propTypes = {};

export default memo(RainbowStoryCard);
