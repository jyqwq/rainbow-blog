import styled from 'styled-components';

const Container = styled.div`
  background-color: #fcfcfe;
  color: #00283a;
  font-style: italic;
  font-size: 16px;
  font-weight: 500;
  :before {
    margin: 0;
    color: #afb42b;
    width: 50px;
    line-height: 55px;
    content: '"';
    position: absolute;
    top: 10px;
    left: 0;
    font-size: 54px;
    font-family: auto;
  }
  :after {
    margin: 0;
    width: 50px;
    color: #afb42b;
    line-height: 55px;
    content: '"';
    position: absolute;
    bottom: -20px;
    right: 10px;
    font-size: 54px;
    font-family: auto;
  }
`;

const Date = styled.div`
  position: absolute;
  right: 60px;
  bottom: 10px;
  font-size: 0.8rem;
  color: #999999;
`;

export default {
  Container,
  Date,
};
