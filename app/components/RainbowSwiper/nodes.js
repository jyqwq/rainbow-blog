import styled from 'styled-components';

const Swiper = styled.div`
  width: 100%;
  height: 700px;
  position: relative;
  z-index: 1;
  overflow: hidden;
  border-radius: 0 0 20px 20px;
  box-shadow: 0 2px 4px -2px rgb(0 0 0 / 15%);

  .swiper {
    width: 100%;
    height: 100%;
    background: #000;
  }

  .swiper-slide {
    font-size: 18px;
    color: #fff;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 40px 60px;
  }

  .parallax-bg {
    position: absolute;
    left: 0;
    top: 0;
    width: 130%;
    height: 100%;
    -webkit-background-size: cover;
    background-size: cover;
    background-position: center;
  }

  .swiper-slide .title {
    font-size: 60px;
    font-weight: 600;
    font-family: 'rainbow';
  }

  .swiper-slide .subtitle {
    font-size: 30px;
    font-family: 'rainbow';
  }

  .swiper-slide .text {
    font-size: 26px;
    line-height: 50px;
    font-family: 'rainbow';
  }

  .swiper-slide .img {
    height: 250px;
    img {
      height: 250px;
      border-radius: 125px;
      box-shadow: 0 0 70px 30px rgb(249, 249, 249) inset;
    }
  }
`;

const SwiperCard = styled.div`
  z-index: -1;
  position: absolute;
  width: 100%;
  height: 700px;
  object-fit: cover;
  object-position: top;
`;

const SwiperCardText = styled.div`
  background-image: linear-gradient(transparent, rgba(10, 15, 20, 0.2));
  z-index: 10;
  position: absolute;
  top: -120px;
  width: 100%;
  height: 800px;
  display: flex;
  align-items: center;
  .ant-row {
    width: 100%;
  }
`;

const BannerImg = styled.img`
  width: 100%;
  height: 100%;
`;

const BannerTitle = styled.div`
  font-size: 3rem;
  font-weight: 600;
  color: white;
`;

const BannerSubTitle = styled.div`
  font-size: 1rem;
  font-weight: 400;
  color: white;
`;

export default {
  Swiper,
  SwiperCard,
  SwiperCardText,
  BannerImg,
  BannerTitle,
  BannerSubTitle,
};
