import React, { memo } from 'react';
// import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { imgBaseUrl } from '../../global-urls';
import Nodes from './nodes';
import { Col, Row } from 'antd';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import SwiperCore, { Parallax, Autoplay } from 'swiper';
import { delay } from 'redux-saga/effects';
SwiperCore.use([Parallax, Autoplay]);

function RainbowSwiper() {
  const data = [
    {
      mainTitle: '纸上的彩虹',
      subTitle: 'Rainbow In Paper',
      text: '有人住高楼，有人在深沟，有人光芒万丈，有人一身锈，世人万千种，浮云莫去求，斯人若彩虹，遇上方知有。',
      img: 'home.webp',
    },
    {
      mainTitle: '手寫的從前',
      subTitle: 'Rainbow In Paper',
      text: '我觉得这个世界美好无比。晴时满树花开，雨天一湖涟漪，阳光席卷城市，微风穿越指间。',
      img: 'swiper01.JPG',
    },
    {
      mainTitle: '斯人若彩虹',
      subTitle: 'Rainbow In Paper',
      text: '人们聚和离，云朵来又往。讲故事的人，总有一个故事不愿讲。时光飞逝，悄悄话变成纸张。',
      img: 'swiper02.JPG',
    },
    {
      mainTitle: '遇上方知有',
      subTitle: 'Rainbow In Paper',
      text: '你无法同时拥有青春和对青春的感受。青春就是这样啊。慌乱和美好，简陋而隆重。匆忙而漫长。久远又熟悉。',
      img: 'swiper03.JPG',
    },
  ];
  return (
    <Nodes.Swiper data-scroll data-scroll-direction="vertical" data-scroll-speed="-1">
      <Swiper speed={1000} parallax autoplay={{ delay: 3000 }} className="mySwiper">
        <div
          slot="container-start"
          className="parallax-bg"
          style={{ backgroundImage: `url(${imgBaseUrl}swiper03.JPG)` }}
          data-swiper-parallax="-23%"
        />
        {data.map(item => (
          <SwiperSlide>
            <Row className="title" data-swiper-parallax="-2000">
              {item.mainTitle}
            </Row>
            <Row className="subtitle" data-swiper-parallax="-1000">
              <Col span={2} />
              <Col>{item.subTitle}</Col>
            </Row>
            <br />
            <br />
            <Row className="text" data-swiper-parallax="-100">
              <Col span={2} />
              <Col span={12}>{item.text}</Col>
              <Col span={10} />
            </Row>
            {/* <br /> */}
            {/* <Row className="img" data-swiper-parallax="-100"> */}
            {/*  <Col span={2} /> */}
            {/*  <Col> */}
            {/*    <img src={`${imgBaseUrl}${item.img}`} alt="" /> */}
            {/*  </Col> */}
            {/* </Row> */}
          </SwiperSlide>
        ))}
      </Swiper>
    </Nodes.Swiper>
  );
}

RainbowSwiper.defaultProps = {};

RainbowSwiper.propTypes = {};

export default memo(RainbowSwiper);
