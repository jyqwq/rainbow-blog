import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowSwiper';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowSwiper component!',
  },
});
