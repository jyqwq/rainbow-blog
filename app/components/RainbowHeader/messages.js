import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowHeader';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowHeader component!',
  },
});
