import styled from 'styled-components';

const Position = styled.div`
  position: relative;
  z-index: 10;
`;

const Container = styled.div`
  opacity: 0.8;
  //position: fixed;
  //left: 0;
  //right: 0;
  border-radius: 20px;
  box-shadow: 0 2px 4px 0 rgb(0 0 0 / 15%);
  z-index: 9;
  background-color: #fcfcfe;
  width: auto;
  max-width: 1880px;
  height: 70px;
  margin: auto;
  text-align: center;
  line-height: 70px;
  :after {
    content: '';
    position: absolute;
    left: 30px;
    height: 8px;
    width: calc(100% - 60px);
    background-color: #fcfcfe;
    border-radius: 0 0 5px 5px;
    opacity: 0.3;
  }
`;

const Content = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  max-width: 1140px;
  .ant-menu {
    background: transparent;
  }
  .ant-menu-horizontal {
    border-bottom: none;
    line-height: 70px;
  }
  .ant-menu-item {
    width: 120px;
    text-align: center;
  }
`;

export default {
  Position,
  Container,
  Content,
};
