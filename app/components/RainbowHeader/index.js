import React, { memo } from 'react';
// import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import { imgBaseUrl } from '../../global-urls';
import { Menu, Row, Col, Button } from 'antd';
import { Link, useLocation } from 'react-router-dom';

function RainbowHeader() {
  const location = useLocation();
  return (
    <Nodes.Position>
      <Nodes.Container>
        <Nodes.Content>
          <Row>
            <Col span={6}>
              <img style={{ height: '60px' }} src={`${imgBaseUrl}header_logo.png`} alt="" />
            </Col>
            <Col span={14}>
              <Menu mode="horizontal" defaultSelectedKeys={[location.pathname]}>
                <Menu.Item key="/">
                  <Link to="/">主页</Link>
                </Menu.Item>
                <Menu.Item key="/blog">
                  <Link to="/blog">博客</Link>
                </Menu.Item>
                <Menu.Item key="/editor">
                  <Link to="/editor">编辑</Link>
                </Menu.Item>
                <Menu.Item key="/ideas">
                  <Link to="/ideas">创意</Link>
                </Menu.Item>
                <Menu.Item key="/selfCenter">
                  <Link to="/selfCenter">个人中心</Link>
                </Menu.Item>
              </Menu>
            </Col>
            <Col span={4}>
              <Button type="primary" shape="round">
                登录
              </Button>
            </Col>
          </Row>
        </Nodes.Content>
      </Nodes.Container>
    </Nodes.Position>
  );
}

RainbowHeader.defaultProps = {};

RainbowHeader.propTypes = {};

export default memo(RainbowHeader);
