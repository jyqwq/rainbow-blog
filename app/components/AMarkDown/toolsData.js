export const toolsData = [
  {
    name: '标题',
    svgHtml: `<svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3 1v13.33M12.997 1v13.33M3 7.665h9.998" stroke="currentColor" stroke-width="1.33" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: '# ',
    endStr: '',
  },
  {
    name: '粗体',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="M24 24c5.506 0 9.969-4.477 9.969-10S29.506 4 24 4H11v20h13zM28.031 44C33.537 44 38 39.523 38 34s-4.463-10-9.969-10H11v20h17.031z" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: '**',
    endStr: '**',
  },
  {
    name: '斜体',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20 6h16M12 42h16M29 5.952 19 42" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: '*',
    endStr: '*',
  },
  {
    name: '引用',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M18.853 9.116c-7.53 4.836-11.714 10.465-12.55 16.887C5 36 13.94 40.893 18.47 36.497 23 32.1 20.285 26.52 17.005 24.994c-3.28-1.525-5.286-.994-4.936-3.032.35-2.039 5.016-7.69 9.116-10.323a.749.749 0 0 0 .114-1.02L20.285 9.3c-.44-.572-.862-.55-1.432-.185zM38.679 9.116c-7.53 4.836-11.714 10.465-12.55 16.887-1.303 9.997 7.637 14.89 12.167 10.494 4.53-4.397 1.815-9.977-1.466-11.503-3.28-1.525-5.286-.994-4.936-3.032.35-2.039 5.017-7.69 9.117-10.323a.749.749 0 0 0 .113-1.02L40.11 9.3c-.44-.572-.862-.55-1.431-.185z" fill="currentColor"></path></svg>`,
    startStr: '> ',
    endStr: '',
  },
  {
    name: '链接',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="m26.24 16.373-9.14-9.14c-2.661-2.661-7.035-2.603-9.769.131-2.733 2.734-2.792 7.107-.13 9.768l7.935 7.936M32.903 23.003l7.935 7.936c2.661 2.66 2.603 7.034-.13 9.768-2.735 2.734-7.108 2.792-9.77.131l-9.14-9.14" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path><path d="M26.11 26.142c2.733-2.734 2.791-7.108.13-9.769M21.799 21.799c-2.734 2.733-2.793 7.107-.131 9.768" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: '[',
    endStr: '](url)',
  },
  {
    name: '图片',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path fill="#fff" fill-opacity=".01" fill-rule="evenodd" d="M0 0h48v48H0z"></path><g transform="translate(5 8)" stroke-width="4" stroke-linejoin="round" stroke="currentColor" fill="none"><path d="M2 0h34a2 2 0 0 1 2 2v28a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z" stroke-linecap="round"></path><circle stroke-linecap="round" cx="9.5" cy="8.5" r="1.5"></circle><path d="m10 16 5 4 6-7 17 13v4a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-4l10-10z"></path></g></svg>`,
    startStr: '![',
    endStr: '](url)',
  },
  {
    name: '代码',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 13 4 25.432 16 37M32 13l12 12.432L32 37" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path><path d="m28 4-7 40" stroke="currentColor" stroke-width="4" stroke-linecap="round"></path></svg>`,
    startStr: '`',
    endStr: '`',
  },
  {
    name: '代码块',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 4c-2 0-5 1-5 5v9c0 3-5 5-5 5s5 2 5 5v11c0 4 3 5 5 5M32 4c2 0 5 1 5 5v9c0 3 5 5 5 5s-5 2-5 5v11c0 4-3 5-5 5" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: `\`\`\`js

`,
    endStr: `\`\`\``,
  },
  {
    name: '无序列表',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 42a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM9 14a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM9 28a4 4 0 1 0 0-8 4 4 0 0 0 0 8z" stroke="currentColor" stroke-width="4" stroke-linejoin="round"></path><path d="M21 24h22M21 38h22M21 10h22" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: '- ',
    endStr: '',
  },
  {
    name: '有序列表',
    svgHtml: `<svg width="1em" height="1em" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 4v9M12 13H6M12 27H6M6 20s3-3 5 0-5 7-5 7M6 34.5s2-3 5-1 0 4.5 0 4.5 3 2.5 0 4.5-5-1-5-1M11 38H9M9 4 6 6M21 24h22M21 38h22M21 10h22" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg>`,
    startStr: '1. ',
    endStr: '',
  },
];
