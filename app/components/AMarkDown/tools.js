export const getSelection = () =>
  window.getSelection ? window.getSelection() : document.selection.createRange();
