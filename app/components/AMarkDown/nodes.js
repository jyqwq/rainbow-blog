import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
`;
const ToolBar = styled.div`
  width: 100%;
  display: flex;
  padding: 4px 12px;
  border-bottom: 1px solid #e1e4e8;
  border-top: 1px solid #e1e4e8;
  background-color: #fafbfc;
  user-select: none;
  overflow: hidden;
`;
const ToolItem = styled.div`
  display: inline-block;
  vertical-align: top;
  cursor: pointer;
  border-radius: 4px;
  margin-left: 6px;
  margin-right: 6px;
  :hover {
    background-color: #e1e4e8;
  }
  svg {
    padding: 4px;
    width: 24px;
    height: 24px;
  }
`;
const TestCursor = styled.div`
  cursor: text;
  height: 30px;
  width: 200px;
`;

export default {
  Container,
  ToolBar,
  ToolItem,
  TestCursor,
};
