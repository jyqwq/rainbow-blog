import { defineMessages } from 'react-intl';

export const scope = 'app.components.AMarkDown';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AMarkDown component!',
  },
});
