import React, { memo, useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { toolsData } from './toolsData';
import Nodes from './nodes';
import Marked from 'marked';
import hljs from 'highlight.js';
import { Row, Col, Input, Tooltip } from 'antd';
import 'highlight.js/styles/monokai-sublime.css';
const { TextArea } = Input;
import { getSelection } from './tools';

function AMarkDown(props) {
  const [articleContent, setArticleContent] = useState(''); //markdown的编辑内容
  const [markdownContent, setMarkdownContent] = useState(''); //html内容
  const renderer = new Marked.Renderer();

  useEffect(() => {
    Marked.setOptions({
      renderer,
      highlight(code) {
        return hljs.highlightAuto(code).value;
      },
      pedantic: false, // 不纠正原始模型任何的不良行为和错误（默认为false）
      gfm: true, // 允许 Git Hub标准的markdown.
      tables: true, // 允许支持表格语法（该选项要求 gfm 为true）
      breaks: false, // 允许回车换行（该选项要求 gfm 为true）
      sanitize: false, // 对输出进行过滤（清理），将忽略任何已经输入的html代码（标签）
      smartLists: true, // 使用比原生markdown更时髦的列表
      smartypants: false, // 使用更为时髦的标点
      xhtml: false,
    });
    // props.componentDidMountAction();
  }, []);

  const changeContent = e => {
    setArticleContent(e.target.value);
    const html = Marked(e.target.value, { renderer });
    setMarkdownContent(html);
  };
  const changePressEnter = e => {
    window.getSelection();
    // e.target.value += '  ';
    console.log(window.getSelection());
    changeContent(e);
  };
  function toolClick(tool) {
    return () => {
      const Selection = getSelection();
      console.log(Selection, Selection.toString());
      const newVal = '';
      // articleContent + tool.startStr + Selection.toString() + tool.endStr;
      Selection.deleteFromDocument();
      setArticleContent(newVal);
      setMarkdownContent(html);
      const html = Marked(newVal, { renderer });
      setMarkdownContent(html);
    };
  }
  return (
    <Nodes.Container>
      {/* <FormattedMessage {...messages.header} /> */}
      <div>
        <Nodes.ToolBar>
          {toolsData.map(item => (
            <Tooltip title={item.name}>
              <Nodes.ToolItem
                key={item.name}
                onClick={toolClick(item)}
                dangerouslySetInnerHTML={{ __html: item.svgHtml }}
              ></Nodes.ToolItem>
            </Tooltip>
          ))}
        </Nodes.ToolBar>
        <div className="markdown-1" onClick={toolClick()}>
          dsdsada
        </div>
        <div className="markdown-2" onClick={toolClick()}>
          dsdsada
        </div>
        <div className="markdown-3" onClick={toolClick()}>
          dsdsada
        </div>
        <Row gutter={5}>
          <Col span={24}>
            <Row gutter={10}>
              <Col span={12}>
                <TextArea
                  className="markdown-content"
                  rows={35}
                  value={articleContent}
                  onChange={changeContent}
                  onPressEnter={changePressEnter}
                  placeholder="编辑内容"
                />
              </Col>
              <Col span={12}>
                <div
                  className="markdown-body"
                  dangerouslySetInnerHTML={{ __html: markdownContent }}
                ></div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Nodes.Container>
  );
}

AMarkDown.defaultProps = {};

AMarkDown.propTypes = {};

export default memo(AMarkDown);
