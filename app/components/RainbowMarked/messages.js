import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowMarked';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowMarked component!',
  },
});
