import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Nodes from './nodes';

function RainbowBaseCard(props) {
  return (
    <Nodes.Container style={props.style} {...props.options} shadow={props.boxShadow}>
      {React.Children.only(props.children)}
    </Nodes.Container>
  );
}

RainbowBaseCard.defaultProps = {
  style: {},
  options: {},
  shadow: false,
};

RainbowBaseCard.propTypes = {
  children: PropTypes.element.isRequired,
  style: PropTypes.object, // 覆盖样式
  options: PropTypes.object, // antd Card配置
  shadow: PropTypes.bool, // 背景阴影
};

export default memo(RainbowBaseCard);
