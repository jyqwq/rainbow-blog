import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowBaseCard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowBaseCard component!',
  },
});
