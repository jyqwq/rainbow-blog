import styled from 'styled-components';
import { Card } from 'antd';

const Container = styled(Card)`
  border-radius: 30px !important;
  box-shadow: 0 2px 4px 0 rgb(0 0 0 / 15%) !important;
  background-color: #fcfcfe !important;
  text-align: center;
  overflow: hidden;
  z-index: 9;
  ${props =>
    props.boxShadow
      ? ":after {\n    content: '';\n    position: absolute;\n    left: 35px;\n    top: -8px;\n    height: 8px;\n    width: calc(100% - 70px);\n    background-color: #fcfcfe;\n    border-radius: 5px 5px 0 0;\n    opacity: 0.3;\n  }"
      : ''}
`;

export default {
  Container,
};
