import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
`;

const Text = styled.p`
  font-size: 2rem;
`;

const Title = styled.p`
  font-size: 1rem;
  text-align: right;
`;

export default {
  Container,Text,Title
};
