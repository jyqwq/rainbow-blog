import React, { memo } from 'react';
// import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import RainbowBaseCard from '../RainbowBaseCard';
import { imgBaseUrl } from '../../global-urls';

function RainbowColorCard() {
  return (
    <RainbowBaseCard
      style={{
        backgroundImage: `url('${imgBaseUrl}color_bg.png'),url('${imgBaseUrl}color_bg_top.png')`,
        backgroundRepeat: 'repeat,repeat-x',
        backgroundPosition: '0 0,0 0',
        backgroundSize: 'auto,auto 1.5rem',
        margin: '0 15px',
        paddingTop: '20px',
        borderRadius: '15px !important',
        backgroundColor: '#f9906f !important',
        borderColor: '#f9906f !important',
      }}
    >
      <Nodes.Container>
        <Nodes.Text>不知乘月几人归</Nodes.Text>
        <Nodes.Text>落月摇情满江树</Nodes.Text>
        <Nodes.Title>张若虚 · 春江花月夜</Nodes.Title>
      </Nodes.Container>
    </RainbowBaseCard>
  );
}

RainbowColorCard.defaultProps = {};

RainbowColorCard.propTypes = {};

export default memo(RainbowColorCard);
