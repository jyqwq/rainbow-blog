import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowColorCard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowColorCard component!',
  },
});
