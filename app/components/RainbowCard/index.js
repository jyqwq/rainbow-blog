import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Divider } from 'antd';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Nodes from './nodes';
import RainbowBaseCard from '../RainbowBaseCard';

function RainbowCard(props) {
  return (
    <RainbowBaseCard style={props.style} shadow>
      <Nodes.Container>
        <div>标题</div>
        <Divider dashed />
        <div>作者</div>
      </Nodes.Container>
    </RainbowBaseCard>
  );
}

RainbowCard.defaultProps = {
  style: {},
};

RainbowCard.propTypes = {
  style: PropTypes.object,
};

export default memo(RainbowCard);
