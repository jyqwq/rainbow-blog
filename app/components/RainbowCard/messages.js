import { defineMessages } from 'react-intl';

export const scope = 'app.components.RainbowCard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RainbowCard component!',
  },
});
