import React, { useRef } from 'react';
import GlobalStyle from '../../global-styles';
import { Switch, Route } from 'react-router-dom';
import { LocomotiveScrollProvider } from 'react-locomotive-scroll';
import RainbowHeader from '../../components/RainbowHeader';
import Home from '../Home/Loadable';
import Blog from '../Blog/Loadable';
import Editor from '../Editor/Loadable';
import Ideas from '../Ideas/Loadable';
import SelfCenter from '../SelfCenter/Loadable';
import { notification } from 'antd';

export default function App() {
  const containerRef = useRef(null);
  notification.info({
    message: '纸上的彩虹',
    description: (
      <span>
        此网站仍在开发中并可能长期处在开发中
        <br />
        联系我们：B站ID：
        <a href="https://space.bilibili.com/21648642">纸上的彩虹丶</a>
      </span>
    ),
    className: 'notification-class',
    style: {
      width: 400,
    },
  });
  return (
    <LocomotiveScrollProvider
      options={{
        smooth: true,
      }}
      containerRef={containerRef}
      watch={[]}
    >
      <div data-scroll-container ref={containerRef}>
        <RainbowHeader />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/editor" component={Editor} />
          <Route exact path="/blog" component={Blog} />
          <Route exact path="/ideas" component={Ideas} />
          <Route exact path="/selfCenter" component={SelfCenter} />
        </Switch>
        <GlobalStyle />
      </div>
    </LocomotiveScrollProvider>
  );
}
