import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  padding-bottom: 20px;
`;

const Content = styled.div`
  padding: 10px;
`;

export default {
  Container,
  Content,
};
