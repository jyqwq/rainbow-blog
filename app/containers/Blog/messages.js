import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Blog';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Blog container!',
    description: 'This is the Blog container!',
  },
});
