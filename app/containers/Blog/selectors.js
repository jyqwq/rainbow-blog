import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectBlogDomain = state => state.blog || initialState;

const makeSelectBlog = () => createSelector(selectBlogDomain, subState => subState);

export default makeSelectBlog;
export { selectBlogDomain, makeSelectBlog };
