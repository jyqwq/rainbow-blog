import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from '../../utils/injectSaga';
import { useInjectReducer } from '../../utils/injectReducer';
import makeSelectEditor from './selectors';
import reducer from './reducer';
import { componentDidMountAction } from './actions';
import saga from './saga';
import messages from './messages';
import Nodes from './nodes';
import AMarkDown from '../../components/AMarkDown';
import { Helmet } from 'react-helmet';

export function Editor(props) {
  useInjectReducer({ key: 'editor', reducer });
  useInjectSaga({ key: 'editor', saga });
  useEffect(() => {
    props.componentDidMountAction();
  }, []);

  return (
    <Nodes.Container id="Editor" data-scroll-section>
      <FormattedMessage {...messages.header}>
        {title => (
          <Helmet>
            <title>{title}</title>
            <meta name="description" content="Description of Editor" />
          </Helmet>
        )}
      </FormattedMessage>
      <AMarkDown />
    </Nodes.Container>
  );
}

Editor.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  editor: makeSelectEditor(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    componentDidMountAction: () => dispatch(componentDidMountAction()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect, memo)(Editor);
