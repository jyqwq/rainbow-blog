import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectEditorDomain = state => state.editor || initialState;

const makeSelectEditor = () => createSelector(selectEditorDomain, subState => subState);

export default makeSelectEditor;
export { selectEditorDomain, makeSelectEditor };
