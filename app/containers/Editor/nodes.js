import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  padding-top: 90px;
  position: relative;
`;

export default {
  Container,
};
