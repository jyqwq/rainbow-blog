import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Editor';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Editor container!',
    description: 'This is the Editor container!',
  },
});
