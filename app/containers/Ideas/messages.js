import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Ideas';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Ideas container!',
    description: 'This is the Ideas container!',
  },
});
