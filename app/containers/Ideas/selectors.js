import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectIdeasDomain = state => state.ideas || initialState;

const makeSelectIdeas = () => createSelector(selectIdeasDomain, subState => subState);

export default makeSelectIdeas;
export { selectIdeasDomain, makeSelectIdeas };
