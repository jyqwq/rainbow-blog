import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from '../../utils/injectSaga';
import { useInjectReducer } from '../../utils/injectReducer';
import makeSelectIdeas from './selectors';
import reducer from './reducer';
import { componentDidMountAction } from './actions';
import saga from './saga';
import messages from './messages';
import Nodes from './nodes';
import { Row, Col } from 'antd';
import RainbowColorCard from '../../components/RainbowColorCard';

export function Ideas(props) {
  useInjectReducer({ key: 'ideas', reducer });
  useInjectSaga({ key: 'ideas', saga });
  useEffect(() => {
    props.componentDidMountAction();
  }, []);

  return (
    <Nodes.Container data-scroll-section>
      <FormattedMessage {...messages.header}>
        {title => (
          <Helmet>
            <title>{title}</title>
            <meta name="description" content="Description of Home" />
          </Helmet>
        )}
      </FormattedMessage>
      <Row>
        <Col span={6}>
          <RainbowColorCard />
        </Col>
        <Col span={6}>
          <RainbowColorCard />
        </Col>
        <Col span={6}>
          <RainbowColorCard />
        </Col>
        <Col span={6}>
          <RainbowColorCard />
        </Col>
      </Row>
    </Nodes.Container>
  );
}

Ideas.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ideas: makeSelectIdeas(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    componentDidMountAction: () => dispatch(componentDidMountAction()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect, memo)(Ideas);
