import styled from 'styled-components';
import { imgBaseUrl } from '../../global-urls';

const Container = styled.div`
  width: 100%;
  padding-top: 100px;
  background-image: url('${imgBaseUrl}color_bg.png'), url('${imgBaseUrl}color_bg_top.png');
  background-repeat: repeat, repeat-x;
  background-position: 0 0, 0 0;
  background-size: auto, auto 3rem;
  transition: background-color 1.6s ease 0s;
  min-height: 100vh;
  font-family: 'rainbow';
`;

export default {
  Container,
};
