import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
`;

const SelfContent = styled.div`
  height: 1000px;
  width: 100%;
  margin: 20px auto;
  max-width: 960px;
  display: flex;
  justify-content: space-between;
`;

const ContentLeft = styled.div`
  flex: 1;

  .content__leftInfo {
    padding: 30px;
    background-color: white;

    .headPortrait {
      overflow: hidden;

      & > div {
        border-radius: 50%;
        overflow: hidden;
        margin-right: 30px;
      }
    }

    .info {
      font-size: 14px;
      color: #72777b;
      flex: 1;

      .userName {
        font-size: 26px;
        color: #000;
        font-weight: 600;
        line-height: 1.2;
      }
    }

    .shareAndEdit {
      display: flex;
      flex-direction: column;
      justify-content: space-between;

      .editBtn {
        color: #3780f7;
        width: 116px;
        height: 32px;
        line-height: 30px;
        text-align: center;
        border: 1px solid #3780f7;
        border-radius: 4px;
        cursor: pointer;

        &:hover {
          color: #5b91ea;
          border: 1px solid #5b91ea;
        }
      }
    }
  }
  .content__leftSpace {
    margin-top: 12px;
    ul {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      font-size: 16px;
      background: #fff;
    }
    li {
      width: 78px;
      height: 50px;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #31445b;
      font-weight: 500;
      cursor: pointer;
      &.active {
        color: #3780f7;
        box-shadow: inset 0 -2px 0 #3780f7;
      }
      &:hover {
        color: #007fff;
      }
    }
  }
`;

const ContentRight = styled.div`
  width: 240px;
  margin-left: 10px;
  .ContentRight {
    flex-direction: column;
    .totalAttention {
      background-color: #fff;
      padding: 14px 0;
      margin-bottom: 12px;
      > div.ant-row {
        text-align: center;
        color: #31445b;
        > div.ant-col {
          flex: 1;
          p:first-child {
            font-size: 16px;
            font-weight: 500;
          }
          p:last-child {
            font-size: 15px;
            font-weight: 600;
          }
        }
      }
      ::after {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        width: 1px;
        height: 50%;
        background-color: #f3f3f4;
        transform: translate(-50%, -50%);
      }
    }
    .collection,
    .attentionLabel,
    .joinTime {
      border-top: 1px solid rgba(230, 230, 231, 0.5);
      padding: 15px 5px;
      display: flex;
      justify-content: space-between;
      font-size: 15px;
      color: #000;
      &.joinTime {
        border-bottom: 1px solid rgba(230, 230, 231, 0.5);
      }
    }
  }
`;

export default {
  Container,
  SelfContent,
  ContentLeft,
  ContentRight,
};
