import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from '../../utils/injectSaga';
import { useInjectReducer } from '../../utils/injectReducer';
import makeSelectSelfCenter from './selectors';
import reducer from './reducer';
import { componentDidMountAction } from './actions';
import saga from './saga';
import Nodes from './nodes';
import { imgBaseUrl } from '../../global-urls';
import { Row, Col, Affix } from 'antd';

let timer = 0;
export function SelfCenter(props) {
  useInjectReducer({ key: 'selfCenter', reducer });
  useInjectSaga({ key: 'selfCenter', saga });
  useEffect(() => {
    props.componentDidMountAction();
  }, []);
  const [container, setContainer] = useState(null);

  return (
    <Nodes.Container id="Blog" data-scroll-section>
      <Helmet>
        <title>SelfCenter</title>
        <meta name="description" content="Description of SelfCenter" />
      </Helmet>
      <Nodes.SelfContent ref={setContainer}>
        <Nodes.ContentLeft>
          <Row className="content__leftInfo">
            <Col onMouseLeave={stopRotate} onClick={rotateImage} className="headPortrait">
              <div>
                <img
                  className="headImg"
                  style={{ width: '90px', height: '90px' }}
                  src={`${imgBaseUrl}home.webp`}
                  alt=""
                />
              </div>
            </Col>
            <Col className="info">
              <Row style={{ marginBottom: '10px' }}>
                <span className="userName">icytail</span>
              </Row>
              <Row style={{ lineHeight: '1.5', marginBottom: '10px' }}>
                <svg
                  data-v-d70dcd60=""
                  width="21"
                  height="18"
                  viewBox="0 0 21 18"
                  className="icon position-icon"
                >
                  <g data-v-d70dcd60="" fill="none" fillRule="evenodd">
                    <path
                      data-v-d70dcd60=""
                      fill="#72777B"
                      d="M3 8.909V6.947a1 1 0 0 1 1-1h13a1 1 0 0 1 1 1V8.92l-6 2.184v-.42c0-.436-.336-.79-.75-.79h-1.5c-.414 0-.75.354-.75.79v.409L3 8.909zm0 .7l6 2.184v.47c0 .436.336.79.75.79h1.5c.414 0 .75-.354.75-.79v-.46l6-2.183V16a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V9.609zm6.75 1.075h1.5v1.58h-1.5v-1.58z"
                    />
                    <path
                      data-v-d70dcd60=""
                      stroke="#72777B"
                      d="M7.5 5.213V4A1.5 1.5 0 0 1 9 2.5h3A1.5 1.5 0 0 1 13.5 4v1.213"
                    />
                  </g>
                </svg>
                <span style={{ marginLeft: '15px' }}>前端</span>
              </Row>
              <Row>
                <svg
                  data-v-d70dcd60=""
                  width="21"
                  height="18"
                  viewBox="0 0 21 18"
                  className="icon intro-icon"
                >
                  <path
                    fill="#72777B"
                    fillRule="evenodd"
                    d="M4 4h13a1 1 0 0 1 1 1v9a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V5a1 1 0 0 1 1-1zm9 6a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3 3a3 3 0 0 0-6 0h6zM5 7v1h4V7H5zm0 2.5v1h4v-1H5zM5 12v1h4v-1H5z"
                  />
                </svg>
                <span style={{ marginLeft: '15px' }}>不能养老</span>
              </Row>
            </Col>
            <Col className="shareAndEdit">
              <Row>link</Row>
              <Row>
                <div className="editBtn">编辑个人资料</div>
              </Row>
            </Col>
          </Row>
          <div className="content__leftSpace">
            <ul>
              <li className="active">动态</li>
              <li>文章</li>
              <li>专栏</li>
              <li>沸点</li>
              <li>赞</li>
              <li>小册</li>
              <li>更多</li>
            </ul>
          </div>
        </Nodes.ContentLeft>
        <Nodes.ContentRight>
          <Row className="ContentRight">
            <Col className="totalAttention">
              <Row>
                <Col>
                  <p>关注了</p>
                  <p>0</p>
                </Col>
                <Col>
                  <p>关注者</p>
                  <p>0</p>
                </Col>
              </Row>
            </Col>
            <Col className="collection">
              <span>收藏集</span>
              <span>0</span>
            </Col>
            <Col className="attentionLabel">
              <span>关注标签</span>
              <span>14</span>
            </Col>
            <Col className="joinTime">
              <span>加入于</span>
              <span>2020-8-15</span>
            </Col>
          </Row>
        </Nodes.ContentRight>
      </Nodes.SelfContent>
    </Nodes.Container>
  );
}

SelfCenter.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  selfCenter: makeSelectSelfCenter(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    componentDidMountAction: () => dispatch(componentDidMountAction()),
  };
}

function rotateImage() {
  if (timer) return;
  // 元素角度
  let deg = 0;
  // 旋转速度
  let speed = 1;
  const img = document.querySelector('.headImg');
  timer = window.setInterval(() => {
    img.style.transform = `rotate(${deg * speed}deg)`;
    speed *= 1.0005;
    deg += 1;
  }, 1);
}
function stopRotate() {
  if (!timer) return;
  window.clearInterval(timer);
  timer = 0;
  document.querySelector('.headImg').style.transform = 'rotate(0)';
  // document.querySelector('.headImg').style.transition = 'all 1s';
}
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect, memo)(SelfCenter);
