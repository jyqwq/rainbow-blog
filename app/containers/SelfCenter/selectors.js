import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectSelfCenterDomain = state => state.selfCenter || initialState;

const makeSelectSelfCenter = () => createSelector(selectSelfCenterDomain, subState => subState);

export default makeSelectSelfCenter;
export { selectSelfCenterDomain, makeSelectSelfCenter };
