import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { FormattedMessage } from 'react-intl';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import { makeSelectHome } from './selectors';
import { makeSelectLocale } from '../LanguageProvider/selectors';
import { defaultAction } from './actions';
import messages from './messages';
import Nodes from './nodes';
import { changeLocale } from '../LanguageProvider/actions';
import { Row, Col, Divider } from 'antd';
import RainbowSwiper from '../../components/RainbowSwiper';
import RainbowCard from '../../components/RainbowCard';
import RainbowStoryCard from '../../components/RainbowStoryCard';
import RainbowImageCard from '../../components/RainbowImageCard';
import RainbowFooter from '../../components/RainbowFooter';
import RainbowRankCard from '../../components/RainbowRankCard';

export function Home(props) {
  useInjectReducer({ key: 'home', reducer });
  useInjectSaga({ key: 'home', saga });
  useEffect(() => {
    props.defaultAction();
  }, []);
  return (
    <Nodes.Container data-scroll-section>
      <FormattedMessage {...messages.webTitle}>
        {title => (
          <Helmet>
            <title>{title}</title>
            <meta name="description" content="Description of Home" />
          </Helmet>
        )}
      </FormattedMessage>
      <Nodes.Banner>
        <RainbowSwiper />
      </Nodes.Banner>
      <Nodes.CardArea>
        <Row data-scroll data-scroll-direction="vertical" data-scroll-speed="1">
          <Col span={6}>
            <RainbowCard />
          </Col>
          <Col span={6}>
            <RainbowCard />
          </Col>
          <Col span={6}>
            <RainbowCard />
          </Col>
          <Col span={6}>
            <RainbowCard />
          </Col>
        </Row>
        <Divider orientation="left" dashed>
          Story
        </Divider>
        <Row data-scroll data-scroll-direction="vertical" data-scroll-speed="0">
          <Col span={1} />
          <Col span={5}>
            <RainbowRankCard />
          </Col>
          <Col span={1} />
          <Col span={16}>
            <RainbowStoryCard />
            <RainbowStoryCard />
            <RainbowStoryCard />
            <RainbowStoryCard />
          </Col>
          <Col span={1} />
        </Row>
        <Divider orientation="left" dashed>
          Image Card
        </Divider>
        <Row data-scroll data-scroll-direction="vertical" data-scroll-speed="0">
          <Col span={6}>
            <RainbowImageCard />
          </Col>
          <Col span={6}>
            <RainbowImageCard />
          </Col>
          <Col span={6}>
            <RainbowImageCard />
          </Col>
          <Col span={6}>
            <RainbowImageCard />
          </Col>
        </Row>
        <RainbowFooter data-scroll data-scroll-direction="vertical" data-scroll-speed="0" />
        <Divider />
      </Nodes.CardArea>
    </Nodes.Container>
  );
}

// eslint-disable-next-line react/no-typos
Home.PropTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  locale: makeSelectLocale(),
  home: makeSelectHome(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    defaultAction: () => {
      dispatch(defaultAction());
    },
    changeLang: lang => {
      dispatch(changeLocale(lang));
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect, memo)(Home);
