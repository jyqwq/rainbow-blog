import styled from 'styled-components';

/* eslint-disable prettier/prettier */
const Container = styled.div`
  margin: 0 40px;
`;

const Banner = styled.div`
  width: 100%;
  height: 560px;
  position: relative;
  z-index: 1;
  overflow: hidden;
  border-radius: 0 0 20px 20px;
  box-shadow: 0 2px 4px -2px rgb(0 0 0 / 15%);
`;

const CardArea = styled.div`
  width: 100%;
  position: relative;
  z-index: 2;
  text-align: center;
  .ant-col {
    padding: 10px;
  }
`;

const Step = styled.div`
  position: relative;
  z-index: 3;
`;

export default { Container, Banner, CardArea, Step };
